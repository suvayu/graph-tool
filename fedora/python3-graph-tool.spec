%global	srcname graph-tool

Name:		python3-%{srcname}
Version:	2.27
Release:	1%{?dist}
Summary:	graph-tool -- an efficient python module for analysis of graphs
Group:		Development/Libraries
License:	GPLv3+
URL:		http://graph-tool.skewed.de
Source0:	%{srcname}-%{version}.tar.gz

Provides:	%{srcname} = %{version}-%{release}
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	git
BuildRequires:	libtool
BuildRequires:	boost-devel
BuildRequires:	boost-python3-devel
BuildRequires:	python3-devel
BuildRequires:	cairomm-devel
BuildRequires:	python3-cairo-devel
BuildRequires:	CGAL-devel
BuildRequires:	python3-numpy
BuildRequires:	python3-scipy
BuildRequires:	sparsehash-devel
# BuildRequires:	python3-sphinx
Requires:	boost(x86-64) >= 1.54.0
Requires:	boost-coroutine
Requires:	boost-graph
Requires:	boost-iostreams
Requires:	boost-python3
Requires:	boost-regex
Requires:	python(abi) >= 3
Requires:	cairomm
Requires:	python3-cairo
Requires:	CGAL
Requires:	python3-numpy
Requires:	python3-scipy

%description
graph-tool is an efficient python module for manipulation and
statistical analysis of graphs. It contains several general graph
measurements, data structures and algorithms, such as vertex and edge
properties, online graph filtering, nearest neighbour statistics,
clustering, interactive graph layout, random graph generation, detection
of community structure, and more.

Contrary to most other python modules with similar functionality, the
core data structures and algorithms are implemented in C++, making
extensive use of template metaprogramming, based heavily on the Boost
Graph Library. This confers it a level of performance that is
comparable (both in memory usage and computation time) to that of a
pure C/C++ library.

For more information and documentation, please take a look at the
website http://graph-tool.skewed.de.

This package is compiled against Python 3.

# %package	doc
# Summary:	HTML documentation for graph-tool
# Requires:	%{name} = %{version}-%{release}

# %description	doc
# %{summary}.


%prep
%setup -q -n python3-%{srcname}-%{version}


%build
./autogen.sh
%configure PYTHON=python3 --enable-debug --enable-openmp
make -j1

# # build docs
# # make -C doc PYTHONPATH=%{_builddir}/%{name}-%{version}-%{git}/src/
# make -C doc PYTHONPATH=%{_builddir}/%{name}-%{version}/src/


%install
%make_install
rm -rf $(echo %{buildroot}%{_pkgdocdir} | sed -e 's|/[^/]\+$||')/%{srcname}
# mv %{buildroot}/doc/build %{buildroot}%{_pkgdocdir}/html


%files
%doc LICENSE AUTHORS README.md INSTALL
# %{_libdir}/libgraph_tool*.so
%{python3_sitelib}/*
%{_libdir}/pkgconfig/graph-tool*.pc


# %files	doc
# %doc html


%changelog
* Mon Apr  1 2019 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.27-1
- Update to 2.27

* Fri Mar  3 2017 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.21-1
- Update to 2.21

* Tue Nov 29 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.19-1
- Update to 2.19

* Sun Nov 27 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.18-1
- Update to 2.18

* Fri Jul 15 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.17-1
- Update to 2.17

* Tue Apr 26 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.16-1
- Update to 2.16
- Compile against Python 3

* Tue Apr 12 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.14-1
- Update to 2.14

* Mon Apr  4 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.13-6
- Bug fix graph_draw: cherry picked d9bcc66 from master (fixes #285 upstream)

* Tue Mar 29 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.13-5
- Rebuild with sparsehash 2.0.3

* Fri Mar 25 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.13-4
- Fix requires

* Thu Mar 24 2016 Suvayu Ali <Suvayu.Ali@cern.ch> - 2.13-2
- Build 2.13 release
- Disable docs

* Sat Mar 12 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.13-2.20160311.git.f2c93fc
- Move to release-2.13-11-gf2c93fc from master due to build failure

* Sat Mar 12 2016 Suvayu Ali <fatkasuvayu+linux@gmail.com> - 2.13-1
- Package graph-tool for the first time
